using GpuKron
using Test

@testset "GpuKron.jl" begin
    # Testing requires GPUs
    @test_throws bench(A,B,C,dA,dB,dC)
    
    A = rand(4,4); B = rand(4,4); C = zeros(16,16);
    # kron! only works with sparse matrices on GPU
    @test_throws kron!(C,A,B)

    @test 1 == 1
end
