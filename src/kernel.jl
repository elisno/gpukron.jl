"""
    kron!(C::CuSparseMatrixCSC, A::CuSparseMatrixCSC, B::CuSparseMatrixCSC)
    
Computes the Kronecker product of sparse matrices `A` and `B` on the GPU, by storing the product in the sparse matrix `C`, overwriting it's non-zero values.

!!! warning The function does not change values in `C.rowVal` or `C.colptr`. Hence, the sparsity structure of `C` should be precomputed (e.g. with `kron(A,B)`).
"""
function kron!(C::CuSparseMatrixCSC, A::CuSparseMatrixCSC, B::CuSparseMatrixCSC)
    nnzC = nnz(A)*nnz(B)
    mA, nA = size(A); mB, nB = size(B)
    mC, nC = mA*mB, nA*nB

    rowvalC = C.rowVal
    nzvalC = C.nzVal
    colptrC = C.colPtr

    rowvalA = A.rowVal
    nzvalA = A.nzVal
    colptrA = A.colPtr
	
    rowvalB = B.rowVal
    nzvalB = B.nzVal
    colptrB = B.colPtr

    @boundscheck begin
        length(colptrC) == nC+1 || throw(DimensionMismatch("expect C to be preallocated with $(nC+1) colptrs "))
        length(rowvalC) == nnzC || throw(DimensionMismatch("expect C to be preallocated with $(nnzC) rowvals"))
        length(nzvalC) == nnzC || throw(DimensionMismatch("expect C to be preallocated with $(nnzC) nzvals"))
    end

    function kernel(nzvalC, nzvalA, nzvalB, colptrA, colptrB, colptrC)
        i = threadIdx().x
        j = blockIdx().x            
        col = (j-1)*blockDim().x + i

        startA = @inbounds colptrA[j]
        stopA = @inbounds colptrA[j+1] - 1
        lA = stopA - startA + 1
        if lA == 0
            return
        end

        startB = @inbounds colptrB[i]
        stopB = @inbounds colptrB[i+1] - 1
        lB = stopB - startB + 1
        if lB == 0
            return
        end
        ptr_range_count = 0
        @inbounds for ptrA = startA : stopA
            ptrB = startB
            for ptr = ((ptr_range_count)*lB+1):((ptr_range_count+1) *lB)
                ptrC = ptr + colptrC[col] - 1
                @inbounds nzvalC[ptrC] = nzvalA[ptrA] * nzvalB[ptrB]
                ptrB += 1
            end
            ptr_range_count += 1
        end
        return
    end
        
    @cuda threads=nA blocks=nB kernel(nzvalC, nzvalA, nzvalB, colptrA, colptrB, colptrC)

    return C
end
