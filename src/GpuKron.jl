module GpuKron


using CuArrays, CUDAnative, CUDAdrv
using LinearAlgebra
using SparseArrays
import CuArrays.CUSPARSE: CuSparseMatrixCSC

export kron!, bench

include("kernel.jl")

end # module
