"""
    bench(T,N,p)
    
Generate sparse matrices `A`, `B` and `C` with eltype `T` and size `N`x`N`. CSC format is used, where the fraction of non-zero entries is `p`. 
`dA`, `dB` and `dC` are the corresponding sparse matrices on the GPU.

The values of `C` and `dC` are initialized with structural zeroes.
"""
function bench(T,N,p)
        A = sprand(T,N,N,p)
        B = oneunit(A) 
        C = sparse(kron(A,B))
        C .= 0
        dA = CuSparseMatrixCSC(A);
        dB = CuSparseMatrixCSC(B);
        dC = CuSparseMatrixCSC(C);

        return A, B, C, dA, dB, dC
end
