# GpuKron

[![Build Status](https://gitlab.com/elisno/GpuKron.jl/badges/master/build.svg)](https://gitlab.com/elisno/GpuKron.jl/pipelines)
[![Coverage](https://gitlab.com/elisno/GpuKron.jl/badges/master/coverage.svg)](https://gitlab.com/elisno/GpuKron.jl/commits/master)


A package for computing the Kronecker product of sparse matrices, using CSC format, on the GPU.

## Usage
To add this package in Julia, run the following:
```
using Pkg
Pkg.add("https://gitlab.com/elisno/gpukron.jl")
```

or in the Julia REPL, go to the Pkg-shell by typing `]` running

```
add https://gitlab.com/elisno/gpukron.jl
```


Currently, this package exports two functions.

- `bench`, which generates random sparse matrices on the CPU and GPU as well as output matrices for the kronecker products.

- `kron!`, which computes the Kronecker-product in-place. Currently, this only for `CuSparseMatrixCSC`.

## Benchmarks

On `V100` GPUs, the following script is run:

```julia
using GpuKron

for i = 1:7, T = (Float32, Float64, ComplexF32, ComplexF64)
    N = 2^i
    A,B,C, dA, dB, dC = bench(T, N, 0.25)
    println("T= $T, N = $N")
    println("Out-of-place CPU")
    @btime kron($A,$B)
    println("In-place GPU")
    @btime CuArrays.@sync kron!($dC, $dA, $dB) 
    println("")
end
```

Output:

```
T= Float32, N = 2
Out-of-place CPU
  167.810 ns (5 allocations: 352 bytes)
In-place GPU
  32.345 μs (68 allocations: 1.78 KiB)

T= Float64, N = 2
Out-of-place CPU
  189.214 ns (5 allocations: 384 bytes)
In-place GPU
  31.306 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 2
Out-of-place CPU
  169.256 ns (5 allocations: 352 bytes)
In-place GPU
  31.936 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 2
Out-of-place CPU
  165.269 ns (5 allocations: 352 bytes)
In-place GPU
  32.605 μs (68 allocations: 1.78 KiB)

T= Float32, N = 4
Out-of-place CPU
  511.694 ns (5 allocations: 832 bytes)
In-place GPU
  33.230 μs (68 allocations: 1.78 KiB)

T= Float64, N = 4
Out-of-place CPU
  434.758 ns (5 allocations: 832 bytes)
In-place GPU
  29.429 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 4
Out-of-place CPU
  477.209 ns (5 allocations: 832 bytes)
In-place GPU
  32.679 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 4
Out-of-place CPU
  504.539 ns (5 allocations: 1.03 KiB)
In-place GPU
  32.795 μs (68 allocations: 1.78 KiB)

T= Float32, N = 8
Out-of-place CPU
  2.129 μs (5 allocations: 2.42 KiB)
In-place GPU
  42.765 μs (68 allocations: 1.78 KiB)

T= Float64, N = 8
Out-of-place CPU
  1.317 μs (5 allocations: 2.64 KiB)
In-place GPU
  29.804 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 8
Out-of-place CPU
  1.802 μs (5 allocations: 2.95 KiB)
In-place GPU
  33.254 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 8
Out-of-place CPU
  1.586 μs (5 allocations: 3.42 KiB)
In-place GPU
  33.104 μs (68 allocations: 1.78 KiB)

T= Float32, N = 16
Out-of-place CPU
  9.677 μs (5 allocations: 13.19 KiB)
In-place GPU
  33.379 μs (68 allocations: 1.78 KiB)

T= Float64, N = 16
Out-of-place CPU
  11.026 μs (5 allocations: 21.75 KiB)
In-place GPU
  34.947 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 16
Out-of-place CPU
  11.981 μs (5 allocations: 20.25 KiB)
In-place GPU
  34.369 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 16
Out-of-place CPU
  10.646 μs (5 allocations: 23.13 KiB)
In-place GPU
  33.696 μs (68 allocations: 1.78 KiB)

T= Float32, N = 32
Out-of-place CPU
  68.422 μs (7 allocations: 104.03 KiB)
In-place GPU
  36.135 μs (68 allocations: 1.78 KiB)

T= Float64, N = 32
Out-of-place CPU
  65.786 μs (7 allocations: 143.91 KiB)
In-place GPU
  45.911 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 32
Out-of-place CPU
  80.301 μs (7 allocations: 146.41 KiB)
In-place GPU
  45.912 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 32
Out-of-place CPU
  77.292 μs (7 allocations: 194.41 KiB)
In-place GPU
  39.938 μs (68 allocations: 1.78 KiB)

T= Float32, N = 64
Out-of-place CPU
  516.423 μs (8 allocations: 801.86 KiB)
In-place GPU
  39.800 μs (68 allocations: 1.78 KiB)

T= Float64, N = 64
Out-of-place CPU
  503.751 μs (8 allocations: 1.04 MiB)
In-place GPU
  38.930 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 64
Out-of-place CPU
  562.474 μs (8 allocations: 1.02 MiB)
In-place GPU
  43.686 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 64
Out-of-place CPU
  577.157 μs (8 allocations: 1.46 MiB)
In-place GPU
  43.907 μs (68 allocations: 1.78 KiB)

T= Float32, N = 128
Out-of-place CPU
  4.011 ms (8 allocations: 6.04 MiB)
In-place GPU
  92.750 μs (68 allocations: 1.78 KiB)

T= Float64, N = 128
Out-of-place CPU
  3.680 ms (8 allocations: 8.26 MiB)
In-place GPU
  86.804 μs (68 allocations: 1.78 KiB)

T= Complex{Float32}, N = 128
Out-of-place CPU
  4.480 ms (8 allocations: 8.22 MiB)
In-place GPU
  99.782 μs (68 allocations: 1.78 KiB)

T= Complex{Float64}, N = 128
Out-of-place CPU
  4.682 ms (8 allocations: 12.12 MiB)
In-place GPU
  137.280 μs (68 allocations: 1.78 KiB)
```
